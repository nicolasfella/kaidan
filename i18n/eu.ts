<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="eu">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Erabiltzeko erraza den Jabber/XMPP bezero bat</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Lizentzia:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Ikusi iturburu kodea linean</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountDeletionFromClientAndServerConfirmationPage</name>
    <message>
        <source>Delete account completely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">Ezabatu</translation>
    </message>
</context>
<context>
    <name>AccountDeletionFromClientConfirmationPage</name>
    <message>
        <source>Remove account from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
You won&apos;t be able to get your credentials back!
Make sure that you have backed up those if you want to use your account later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountDeletionPage</name>
    <message>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can remove your account &lt;b&gt;%1&lt;/b&gt; only from this app or delete it completely. If you delete your account completely, you won&apos;t be able to use it with another app because it is also removed from the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete completely</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Atzerantz nabigatu</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Pasahitza aldatu</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Egungo pasahitza:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Pasahitz berria:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Pasahitz berria (errepikatu):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Pasahitz berriak ez datoz bat.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Egungo pasahitza ez da balekoa.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Pasahitza aldatzeko konektaturik egon behar duzu.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Pasahitza aldatu ostean, zure gainontzeko gailuetan berriro sartu beharko duzu.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Utzi</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Aldatu</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Message copied to clipboard</source>
        <translation type="vanished">Mezua arbelean kopiatua</translation>
    </message>
    <message>
        <source>Copy Message</source>
        <translation type="vanished">Mezua kopiatu</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation type="vanished">Mezua editatu</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Deskargatu</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Mezua sortu</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Irudia</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Bideoa</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Audioa</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">Dokumentua</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation type="vanished">Beste artxibo bat</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Artxibo bat aukeratu</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>«Spoiler» mezu bat bidali</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>«Spoiler» keinua</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">Itxi</translation>
    </message>
    <message>
        <source>Search up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very high</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Utzi</translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Jarduerak</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Ezin izan da %1 fitxategia gorde</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Deskarga akatsa: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Food</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Hauta ezazu txat bat mezuak bidaltzen hasteko</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Artxibo bat aukeratu</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>go to parent folder</source>
        <translation type="vanished">maila bat goragoko karpetara joan</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Itxi</translation>
    </message>
    <message>
        <source>Go to parent folder</source>
        <translation type="vanished">Karpeta nagusira joan</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Aurrerantz nabigatu</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Back</source>
        <translation type="vanished">Atzera</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation type="vanished">Saioa itxi</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Honi buruz</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Lagunak gonbidatu</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Gonbidapen esteka arbelean kopiatua</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ezarpenak</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Lineaz kanpo</translation>
    </message>
    <message>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation type="unfinished">Konektatzen…</translation>
    </message>
    <message>
        <source>Transfer account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Ezin izan da mezua bidali, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation type="vanished">Ezin izan da kontaktua gehitu, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation type="vanished">Ezin izan da kontaktua ezabatu, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Esteka konektatu ondoren irekiko da.</translation>
    </message>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation type="vanished">Ezin izan da artxiboa bidali, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Saioa hasi</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Hasi saioa zure XMPP kontuan</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation type="vanished">Zure Jabber-IDa:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Zure diaspora*-IDa:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation type="vanished">erabiltzailea@example.org</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">erabiltzailea@diaspora.pod</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation type="vanished">Zure Pasahitza:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Konektatzen…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Konektatu</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="vanished">Erabiltzaile edo pasahitz okerra.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="vanished">Ezin da zerbitzarira konektatu. Mesedez egiaztatu zure internet konexioa.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="vanished">Zerbitzariak ez du konexio segururik onartzen.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="vanished">Akatsa segurtasunez konektatzerakoan.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation type="vanished">Ezin izan da zerbitzariaren helbidea ebatzi. Mesedez egiazta ezazu zure JIDa berriz.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Ezin izan da zerbitzarira konektatu.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="vanished">Autentifikazio protokoloa ez da zerbitzariarekin bateragarria.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation type="vanished">Akats ezezagun bat gertatu da; ikusi xehetasunak log edo erregistroetan.</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Record video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Ezin izan da mezua bidali, konektaturik ez  dagoelako.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Ezin izan da mezua zuzendu konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Container</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="unfinished">Irudia</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished">Audioa</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="unfinished">Bideoa</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recording...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultimediaSettingsPage</name>
    <message>
        <source>Image</source>
        <translation type="obsolete">Irudia</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">Audioa</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Bideoa</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recording... %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Eskuragarri</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Libre berriketarako</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Kanpoan</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Ez molestatu</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Kanpoan denbora luzerako</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Lineaz kanpo</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Errorea</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="unfinished">Erabiltzaile edo pasahitz okerra.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="unfinished">Ezin da zerbitzarira konektatu. Mesedez egiaztatu zure internet konexioa.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="unfinished">Zerbitzariak ez du konexio segururik onartzen.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="unfinished">Akatsa segurtasunez konektatzerakoan.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your server name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="obsolete">Ezin izan da zerbitzarira konektatu.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="unfinished">Autentifikazio protokoloa ez da zerbitzariarekin bateragarria.</translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logging in…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Pasahitza behar bezala aldatu da.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Akatsa pasahitza aldatzean: %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Kontaktu berria gehitu</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Ezizena:</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-IDa:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>erabiltzailea@example.org</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Utzi</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Gehitu</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Honek kontaktuaren presentziara sarbide eskaera bidaliko du.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Aukerako mezua:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Esan nor zaren zure berriketa lagunari.</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Errorea: Mesedez egiaztatu JIDa.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Eskuragarri</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Libre berriketarako</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Kanpoan</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Ez molestatu</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Kanpoan denbora luzerako</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Lineaz kanpo</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Errorea</translation>
    </message>
    <message>
        <source>Invalid</source>
        <translation type="vanished">Baliogabea</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Ezin izan da kontaktua gehitu, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Ezin izan da kontaktua ezabatu, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Konektatzen…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Kontaktuak</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Kontaktu berria gehitu</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Lineaz kanpo</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Delete contact</source>
        <translation>Kontaktua ezabatu</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Utzi</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Ezabatu</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Ziur al zaude &lt;b&gt;%1&lt;/b&gt; kontaktua zure zerrendatik ezabatu nahi duzula?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Utzi</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Cancel</source>
        <translation>Utzi</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Izenburua</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Bidali</translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Change password</source>
        <translation type="unfinished">Pasahitza aldatu</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="unfinished">Zure kontuaren pasahitza aldatzen du. Zure gainontzeko gailuetan berriro sartu beharko duzu.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Ezarpenak</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="vanished">Pasahitza aldatu</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="vanished">Zure kontuaren pasahitza aldatzen du. Zure gainontzeko gailuetan berriro sartu beharko duzu.</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Ezarpenak</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Izen emate eskaera</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Izen emate eskaera jaso duzu &lt;b&gt;%1&lt;/b&gt;-(r)engandik. Onartzen baldin baduzu, kontuak sarbidea izango du zure presentzia egoerara.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Ukatu</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Onartu</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Ezin izan da artxiboa bidali, konektaturik ez zaudelako.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Artxiboa</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Honi buruz</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
